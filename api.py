# coding=utf-8
from flask import Flask
from flask_restful import Resource, Api
from flask.ext.cors import CORS
from flask import request
import psycopg2
import sys

app = Flask(__name__)
CORS(app)
api = Api(app)

db = psycopg2.connect("dbname='mldb' user='postgres' password='hola123' host='localhost'")

class Player_stats(Resource):
    def get(self):
        cursor = db.cursor()
        cursor.execute("")
        rows = cursor.fetchall()
        cursor.close()
        return rows
api.add_resource(Player_stats, '/player_stats')

class Player_eq(Resource):
    def get(self,id_eq):
        arr = []
        year = request.args.get('year')
        cursor = db.cursor()
        query0 = "SELECT jugadores.id_jugador,jugadores.nombre,jugadores.fecha_nac,jugadores.pais_origen FROM jugadores,equipos,jugador_en_equipo WHERE jugadores.id_jugador = jugador_en_equipo.id_jugador AND equipos.id_equipo = jugador_en_equipo.id_equipo AND jugador_en_equipo.stint = 1 AND equipos.id_equipo = '%s' AND jugador_en_equipo.año = '%s';"%(str(id_eq),str(year))
	print query0
        cursor.execute(query0)
        xrows = cursor.fetchall()
        for row in xrows:
            arr.append({'id_jugador':row[0],'nombre':row[1],'fecha_nac':row[2],'pais_origen':row[3]})
        cursor.close()
        db.commit()
	return arr

api.add_resource(Player_eq, '/player_eq/<id_eq>')




# Consulta: Equipo ganador de liga

class Team_W(Resource):
    def get(self,id_liga):
        cursor = db.cursor()
	query = "select equipos.id_equipo,equipos.nombre, titulos_por_equipo.año from equipos,titulos_por_equipo where titulos_por_equipo.id_ganador = equipos.id_equipo AND titulos_por_equipo.id_liga = '%s';"%(str(id_liga))
	print query
        cursor.execute(query)
        xrows = cursor.fetchall()
        arr = []
        for row in xrows:
            arr.append({'team_id':row[0],'team':row[1],'year':row[2]})
        cursor.close()
        return arr
api.add_resource(Team_W, '/team_w/<id_liga>')

# Equipos en liga por año

class Team_L(Resource):
    def get(self,id_liga):
        year = request.args.get('year')
        cursor = db.cursor()
        query = "select equipos.nombre,equipos.division,estadios.nombre,estadios.estado,estadios.ciudad,equipos.id_equipo from estadios,equipo_en_estadio, equipos,equipo_en_liga, ligas where equipos.id_equipo = equipo_en_estadio.id_equipo AND equipo_en_estadio.id_estadio = estadios.id_estadio AND equipos.id_equipo = equipo_en_liga.id_equipo AND equipo_en_liga.id_liga = ligas.id_liga AND ligas.id_liga = '%s' AND equipo_en_liga.año = '%s'"%(str(id_liga),str(year))
	print query
        cursor.execute(query)
        xrows = cursor.fetchall()
        arr = []
        for row in xrows:
            arr.append({'team':row[0],'div':row[1],'park':row[2],'state':row[3],'city':row[4],'team_id':row[5]})
        cursor.close()
        return arr
api.add_resource(Team_L, '/team_l/<id_liga>')

# ------------------------------------------------------------------------------

class Equipos_por_ano(Resource):
    def get(self,year):
        cursor = db.cursor()
        query = "select equipos.nombre,estadios.estado,estadios.ciudad,estadios.nombre,ligas.nombre,equipos.division,managers.nombre,equipo_en_liga.partidos_ganados,equipo_en_liga.partidos_perdidos,equipos.id_equipo from equipos,equipo_en_liga,ligas,equipo_en_estadio,estadios,manager_de_equipo,managers where managers.id_manager = manager_de_equipo.id_manager and manager_de_equipo.id_equipo = equipos.id_equipo and equipo_en_liga.id_equipo=equipos.id_equipo and ligas.id_liga=equipo_en_liga.id_liga and equipo_en_estadio.id_equipo=equipos.id_equipo and equipo_en_estadio.id_estadio=estadios.id_estadio and ligas.id_liga<>'ALCS' and ligas.id_liga<>'NLCS' and ligas.id_liga<>'WS' and ligas.id_liga<>'ALDS' and ligas.id_liga<>'NLDS' and equipo_en_liga.año='%s' and manager_de_equipo.año='%s';"%(str(year),str(year))
	print query
        cursor.execute(query)
        xrows = cursor.fetchall()
        arr = []
        for row in xrows:
            arr.append({'name':row[0],'state':row[1],'city':row[2],'liga':row[4],'div':row[5],'park':row[3],'man':row[6],'win':row[7],'los':row[8],'team_id':row[9]})
        cursor.close()
        return arr
api.add_resource(Equipos_por_ano,'/teams/<year>')

class Equipo_Dat(Resource):
    def get(self,id_equipo):
        arr = {}
        year = request.args.get('year')
        cursor = db.cursor()
        verify = "SELECT COUNT(1) FROM titulos_por_equipo WHERE id_ganador = '%s' AND año = '%s'"%(str(id_equipo),str(year))
        cursor.execute(verify)
        answer = cursor.fetchone()
        db.commit()
        query1 = "SELECT equipos.nombre,estadios.estado,estadios.ciudad,estadios.nombre,managers.nombre FROM equipos,estadios,equipo_en_estadio,managers,manager_de_equipo WHERE equipos.id_equipo = equipo_en_estadio.id_equipo AND estadios.id_estadio = equipo_en_estadio.id_estadio AND managers.id_manager = manager_de_equipo.id_manager AND manager_de_equipo.id_equipo = equipos.id_equipo AND equipos.id_equipo = '%s' AND manager_de_equipo.año = '%s'"%(str(id_equipo),str(year))
	print query1
        cursor.execute(query1)
        answer2 = cursor.fetchone()
        db.commit()
        arr['nombre_eq'] = str(answer2[0])
        arr['estado'] = str(answer2[1])
        arr['ciudad'] = str(answer2[2])
        arr['nombre_es'] = str(answer2[3])
        arr['nombre_ma'] = str(answer2[4])
        arr['titulos'] = str(answer[0])
        return arr
api.add_resource(Equipo_Dat,'/equipo_dat/<id_equipo>')

#------------------------------------------------------------------------------
class StatsB(Resource):
    def get(self):
        cursor = db.cursor()
        query = "SELECT jugadores.nombre,j_en_e.id_equipo,s_BG.valor,s_AB.valor,s_BR.valor,s_BH.valor,s_2B.valor,s_3B.valor,s_BHR.valor,s_RBI.valor,s_BBB.valor,s_BSO.valor,s_SB.valor,s_CS.valor,s_avg.valor,jugadores.id_jugador FROM jugadores, (select id_jugador,id_equipo from jugador_en_equipo WHERE stint=1 group by id_jugador,id_equipo) as j_en_e,(SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='BG' group by stat_por_jugador.id_jugador) AS s_BG,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='AB' group by stat_por_jugador.id_jugador) AS s_AB,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='BR' group by stat_por_jugador.id_jugador) AS s_BR,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='BH' group by stat_por_jugador.id_jugador) AS s_BH,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='2B' group by stat_por_jugador.id_jugador) AS s_2B,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='3B' group by stat_por_jugador.id_jugador) AS s_3B,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='BHR' group by stat_por_jugador.id_jugador) AS s_BHR,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='RBI' group by stat_por_jugador.id_jugador) AS s_RBI,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='BBB' group by stat_por_jugador.id_jugador) AS s_BBB,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='BSO' group by stat_por_jugador.id_jugador) AS s_BSO,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='SB' group by stat_por_jugador.id_jugador) AS s_SB,(SELECT stat_por_jugador.id_jugador,sum(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='CS' group by stat_por_jugador.id_jugador) AS s_CS,(SELECT stat_AB.id_jugador AS id_jugador, stat_BH.valor_stat::float/stat_AB.valor_stat::float AS valor FROM (SELECT stat.id_jugador,sum(stat.valor_stat) as valor_stat FROM (SELECT * FROM stat_por_jugador WHERE valor_stat!=0) AS stat WHERE stat.id_stat='AB' group by stat.id_jugador) AS stat_AB,(SELECT stat.id_jugador,sum(stat.valor_stat) as valor_stat FROM (SELECT * FROM stat_por_jugador WHERE valor_stat!=0) AS stat WHERE stat.id_stat='BH' group by stat.id_jugador) AS stat_BH WHERE stat_AB.id_jugador=stat_BH.id_jugador) AS s_avg WHERE jugadores.id_jugador=j_en_e.id_jugador AND jugadores.id_jugador=s_BG.id_jugador AND jugadores.id_jugador=s_AB.id_jugador AND jugadores.id_jugador=s_BR.id_jugador AND jugadores.id_jugador=s_BH.id_jugador AND jugadores.id_jugador=s_2B.id_jugador AND jugadores.id_jugador=s_3B.id_jugador AND jugadores.id_jugador=s_BHR.id_jugador AND jugadores.id_jugador=s_RBI.id_jugador AND jugadores.id_jugador=s_BBB.id_jugador AND jugadores.id_jugador=s_BSO.id_jugador AND jugadores.id_jugador=s_SB.id_jugador AND jugadores.id_jugador=s_CS.id_jugador AND jugadores.id_jugador=s_avg.id_jugador;"
	print query
        cursor.execute(query)
        xrows = cursor.fetchall()
        arr = []
        for row in xrows:
            arr.append({'name':row[0],'team':row[1],'G':row[2],'AB':row[3],'R':row[4],'H':row[5],'tB':row[6],'ttB':row[7],'HR':row[8],'RBI':row[9],'BB':row[10],'SO':row[11],'SB':row[12],'CS':row[13],'AVG':"%.3f"%row[14],'player_id':row[15],})
        cursor.close()
        return arr
api.add_resource(StatsB,'/stats_b')

class StatsP(Resource):
    def get(self):
        cursor = db.cursor()
        query = "SELECT jugadores.nombre,j_en_e.id_equipo,s_W.valor,s_L.valor,s_ERA.valor,s_PG.valor,s_IP.valor,s_PH.valor,s_PR.valor,s_ER.valor,s_PHR.valor,s_PBB.valor,s_PSO.valor,jugadores.id_jugador FROM jugadores,(select id_jugador,id_equipo from jugador_en_equipo WHERE stint=1 group by id_jugador,id_equipo) as j_en_e, (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='W' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_W,  (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='L' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_L,  (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='PG' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_PG,  (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='IP' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_IP,  (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='PH' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_PH,  (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='PR' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_PR,  (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='ER' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_ER,  (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='PHR' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_PHR,  (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='PBB' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_PBB,  (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='PSO' AND stat_por_jugador.id_rol='P' group by stat_por_jugador.id_jugador) AS s_PSO, (SELECT stat_ER.id_jugador AS id_jugador, 9*stat_ER.valor::float/stat_IP.valor::float AS valor FROM (SELECT stat.id_jugador,sum(stat.valor_stat) as valor FROM (SELECT * FROM stat_por_jugador WHERE valor_stat!=0) AS stat WHERE stat.id_stat='IP' group by stat.id_jugador) AS stat_IP,(SELECT stat.id_jugador,sum(stat.valor_stat) as valor FROM stat_por_jugador AS stat WHERE stat.id_stat='ER' group by stat.id_jugador) AS stat_ER WHERE stat_ER.id_jugador=stat_IP.id_jugador) as s_ERA WHERE jugadores.id_jugador=j_en_e.id_jugador AND jugadores.id_jugador=s_W.id_jugador AND jugadores.id_jugador=s_L.id_jugador AND jugadores.id_jugador=s_ERA.id_jugador AND jugadores.id_jugador=s_PG.id_jugador AND jugadores.id_jugador=s_IP.id_jugador AND jugadores.id_jugador=s_PH.id_jugador AND jugadores.id_jugador=s_PR.id_jugador AND jugadores.id_jugador=s_ER.id_jugador AND jugadores.id_jugador=s_PHR.id_jugador AND jugadores.id_jugador=s_PBB.id_jugador AND jugadores.id_jugador=s_PSO.id_jugador;"
	print query
        cursor.execute(query)
        xrows = cursor.fetchall()
        arr = []
        for row in xrows:
            arr.append({'name':row[0],'team':row[1],'W':row[2],'L':row[3],'ERA':"%.3f"%row[4],'G':row[5],'IP':row[6],'H':row[7],'R':row[8],'ER':row[9],'HR':row[10],'BB':row[11],'SO':row[12],'player_id':row[13],})
        cursor.close()
        return arr
api.add_resource(StatsP,'/stats_p')

class StatsF(Resource):
    def get(self):
        cursor = db.cursor()
        query = "SELECT jugadores.nombre,j_en_e.id_equipo,s_FG.valor,s_A.valor,s_E.valor,s_DP.valor,jugadores.id_jugador FROM jugadores,(select id_jugador,id_equipo from jugador_en_equipo WHERE stint=1  group by id_jugador,id_equipo) as j_en_e, (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='FG' AND stat_por_jugador.id_rol='F' group by stat_por_jugador.id_jugador) AS s_FG, (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='A' AND stat_por_jugador.id_rol='F' group by stat_por_jugador.id_jugador) AS s_A, (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='E' AND stat_por_jugador.id_rol='F' group by stat_por_jugador.id_jugador) AS s_E, (SELECT stat_por_jugador.id_jugador,SUM(stat_por_jugador.valor_stat) as valor FROM stat_por_jugador WHERE stat_por_jugador.id_stat='DP' AND stat_por_jugador.id_rol='F' group by stat_por_jugador.id_jugador) AS s_DP WHERE jugadores.id_jugador=j_en_e.id_jugador AND jugadores.id_jugador=s_FG.id_jugador AND jugadores.id_jugador=s_A.id_jugador AND jugadores.id_jugador=s_E.id_jugador AND jugadores.id_jugador=s_DP.id_jugador;"
	print query
        cursor.execute(query)
        xrows = cursor.fetchall()
        arr = []
        for row in xrows:
            arr.append({'name':row[0],'team':row[1],'G':row[2],'A':row[3],'E':row[4],'DP':row[5],'player_id':row[6]})
        cursor.close()
        return arr
api.add_resource(StatsF,'/stats_f')

# -----------------------------------------

class Delete_player(Resource):
    def delete(self,nombre):
        arr = {}
    	cursor = db.cursor()
    	verify = "SELECT COUNT(1) FROM jugadores WHERE jugadores.nombre = '%s'"%(str(nombre))
    	cursor.execute(verify)
    	answer = cursor.fetchone()
    	db.commit()
    	if(str(answer[0]) == "1"):
            query0 = "SELECT jugadores.id_jugador FROM jugadores WHERE jugadores.nombre = '%s'"%(nombre)
	    print query0
            cursor.execute(query0)
            id_p = cursor.fetchone()
            id_jugador = str(id_p[0])
            db.commit()
            query1 = "DELETE FROM jugadores WHERE id_jugador = '%s'"%(id_jugador)
	    print query1
            cursor.execute(query1)
            db.commit()
            cursor.close()
            arr['message']='Jugador eliminado con exito'
    	else:
            cursor.close()
            arr['message']='Jugador no existe'
    	return arr
api.add_resource(Delete_player,'/delete_player/<nombre>')

class Update_player(Resource):
    def get(self,nombre):
        arr = {}
        team = request.args.get('team')
        cursor = db.cursor()
        verify = "SELECT COUNT(1) FROM jugadores WHERE nombre = '%s'"%(str(nombre))
        cursor.execute(verify)
        answer = cursor.fetchone()
        db.commit()
        verify2 = "SELECT COUNT(1) FROM equipos WHERE nombre = '%s';"%(str(team))
        cursor.execute(verify2)
        answer2 = cursor.fetchone()
        db.commit()
        if(str(answer[0]) == "1" and str(answer2[0]) == "1"):
            query1 = "SELECT jugadores.id_jugador,equipos.id_equipo FROM jugadores,equipos,jugador_en_equipo WHERE jugadores.id_jugador = jugador_en_equipo.id_jugador AND equipos.id_equipo = jugador_en_equipo.id_equipo AND jugadores.nombre = '%s' GROUP BY jugadores.id_jugador,equipos.id_equipo;"%(nombre)
	    print query1
            cursor.execute(query1)
            rows = cursor.fetchone()
            db.commit()
            query2 = "SELECT equipos.id_equipo FROM equipos WHERE equipos.nombre = '%s';"%(team)
	    print query2
            cursor.execute(query2)
            rows2 = cursor.fetchone()
            db.commit()
            query3 = "UPDATE jugador_en_equipo SET id_equipo = '%s' WHERE id_jugador = '%s' AND id_equipo = '%s';"%(str(rows2[0]),str(rows[0]),str(rows[1]))
	    print query3
            cursor.execute(query3)
            db.commit()
            arr['message'] = 'Cambio de equipo realizado con exito'
            return arr
        else:
            arr['message'] = 'Jugador o equipo no existente'
            return arr
api.add_resource(Update_player,'/update_player/<nombre>')

class Insert_player(Resource):
    def get(self,player):
        arr = {}
        cursor = db.cursor()
        ano = request.args.get('playerYear')
        pais = request.args.get('playerCountry')
        team = request.args.get('team')
        year = request.args.get('teamYear')
        posicion = request.args.get('pos')
        query0 = "SELECT COUNT(1) FROM equipos WHERE nombre = '%s';"%(str(team))
        cursor.execute(query0)
        answer = cursor.fetchone()
        db.commit()
        query = "SELECT COUNT(1) FROM jugadores WHERE nombre = '%s';"%(str(player))
        cursor.execute(query)
        answer1 = cursor.fetchone()
        db.commit()
        if(str(answer[0]) == "1" and str(answer1[0]) == "0"):
            query1 = "SELECT id_equipo FROM equipos WHERE nombre = '%s'"%(str(team))
            cursor.execute(query1)
            answer2 = cursor.fetchone()
            db.commit()
            a = player.lower()
	    try:
            	id_player = a.split(' ')[1][:5]+a.split(' ')[0][:2]+'01'
	    except:
		id_player = a+'01'
            query2 = "INSERT INTO jugadores (id_jugador,nombre,fecha_nac,pais_origen) VALUES ('%s','%s','%s','%s')"%(str(id_player),str(player),str(ano)[:4],str(pais))
	    print query2
            cursor.execute(query2)
            db.commit()
            query3 = "INSERT INTO jugador_en_equipo (id_equipo,id_jugador,año,posicion,stint) VALUES ('%s','%s','%s','%s',1)"%(str(answer2[0]),str(id_player),str(year)[:4],str(posicion))
	    print query3
            cursor.execute(query3)
            db.commit()
            cursor.close()
            arr['message'] = 'Jugador ingresado con exito'
        else:
            cursor.close()
            arr['message'] = 'Equipo no existente o jugador existente'
        return arr
api.add_resource(Insert_player,'/insert_player/<player>')

class Insert_stat_player(Resource):
    def get(self,nombre):
        arr = {}
        id_rol = request.args.get('id_rol')
        id_stat = request.args.get('id_stat')
        valor = request.args.get('valor')
        ano = request.args.get('year')
        cursor = db.cursor()
        verify0 = "SELECT COUNT(1) FROM jugadores WHERE nombre = '%s';"%(str(nombre))
        cursor.execute(verify0)
        answer0 = cursor.fetchone()
        db.commit()
        verify1 = "SELECT COUNT(1) FROM roles WHERE id_rol = '%s';"%(str(id_rol))
        cursor.execute(verify1)
        answer1 = cursor.fetchone()
        db.commit()
        verify2 = "SELECT COUNT(1) FROM stats WHERE id_stat = '%s';"%(str(id_stat))
        cursor.execute(verify2)
        answer2 = cursor.fetchone()
        db.commit()
        if(str(answer0[0]) == "1"):
            if(str(answer1[0]) == "1"):
                if(str(answer2[0]) == "1"):
                    if(str(valor) >= "0"):
                        query3 = "SELECT id_jugador FROM jugadores WHERE nombre = '%s'"%(str(nombre))
                        cursor.execute(query3)
                        answer3 = cursor.fetchone()
                        db.commit()
			verify = "SELECT COUNT(1) FROM stat_por_jugador WHERE id_JUGADOR='%s' AND id_stat='%s' AND año='%s';"%(str(answer3[0]),str(id_stat),str(ano))
			cursor.execute(verify)
			answer = cursor.fetchone()
			db.commit()
			if(str(answer[0]) == "0"):
                        	query4 = "INSERT INTO stat_por_jugador (id_jugador,id_stat,id_rol,valor_stat,año) VALUES ('%s','%s','%s','%s','%s')"%(str(answer3[0]),str(id_stat),str(id_rol),int(valor),str(ano)[:4])
				print query4
                        	cursor.execute(query4)
                        	db.commit()
                        	arr['message'] = 'ingresado con exito'
			else:
				arr['message'] = 'Estadistica ya existente'
                    else:
                        arr['message'] = 'Valor invalido'
                else:
                    arr['message'] = 'Stat invalido'
            else:
                arr['message'] = 'Rol invalido'
        else:
            arr['message'] = 'Jugador no existente'
        return arr
api.add_resource(Insert_stat_player,'/insert_stat_player/<nombre>')

class Player_data(Resource):
    def get(self,id_player):
        arr = {}
        cursor = db.cursor()
        query0 = "SELECT COUNT(1) FROM jugadores WHERE id_jugador = '%s';"%(str(id_player))
        cursor.execute(query0)
        answer1 = cursor.fetchone()
        db.commit()
        if(str(answer1[0]) == "1"):
            query1 = "SELECT nombre,fecha_nac,pais_origen FROM jugadores WHERE id_jugador = '%s';"%(str(id_player))
	    print query1
            cursor.execute(query1)
            answer = cursor.fetchone()
            db.commit()
            arr['nombre'] = answer[0]
            arr['fecha_nac'] = answer[1]
            arr['pais_origen'] = answer[2]
        return arr
api.add_resource(Player_data,'/player_data/<id_player>')

class Manager_year(Resource):
    def get(self,year):
        arr = []
        cursor = db.cursor()
        query = "select managers.nombre,managers.fecha_nac,managers.pais_origen,equipos.nombre FROM equipos,managers,manager_de_equipo WHERE managers.id_manager = manager_de_equipo.id_manager AND manager_de_equipo.id_equipo = equipos.id_equipo AND manager_de_equipo.año = '%s'"%(str(year))
	print query
        cursor.execute(query)
        answer = cursor.fetchall()
        db.commit()
        for row in answer:
            arr.append({'name':row[0],'fecha_nac':row[1],'pais_origen':row[2],'nombre_eq':row[3]})
        cursor.close()
        return arr
api.add_resource(Manager_year,'/manager_year/<year>')

class Random_player(Resource):
    def get(self):
        arr = {}
        cursor = db.cursor()
        query = "SELECT jugadores.id_jugador,jugadores.nombre,jugadores.fecha_nac,jugadores.pais_origen,equipos.nombre from jugadores,jugador_en_equipo,equipos WHERE equipos.id_equipo = jugador_en_equipo.id_equipo AND jugador_en_equipo.id_jugador = jugadores.id_jugador AND jugador_en_equipo.stint = 1 ORDER BY RANDOM() LIMIT 1;"
	print query
        cursor.execute(query)
        answer = cursor.fetchone()
        db.commit()
        arr['id_jugador'] = answer[0]
        arr['nombre'] = answer[1]
        arr['fecha_nac'] = answer[2]
        arr['pais_origen'] = answer[3]
	arr['nombre_eq'] = answer[4]
        cursor.close()
        return arr
api.add_resource(Random_player,'/random_player/')

class Random_team(Resource):
    def get(self):
        arr = {}
        cursor = db.cursor()
        query = "SELECT equipos.nombre,estadios.estado,estadios.ciudad,estadios.nombre,managers.nombre FROM equipos,estadios,equipo_en_estadio,managers,manager_de_equipo WHERE equipos.id_equipo = equipo_en_estadio.id_equipo AND estadios.id_estadio = equipo_en_estadio.id_estadio AND managers.id_manager = manager_de_equipo.id_manager AND manager_de_equipo.id_equipo = equipos.id_equipo AND manager_de_equipo.año = '2015' ORDER BY RANDOM() LIMIT 1;"
	print query
        cursor.execute(query)
        answer = cursor.fetchone()
        db.commit()
        arr['nombre'] = answer[0]
        arr['estado'] = answer[1]
        arr['ciudad'] = answer[2]
        arr['nombre_es'] = answer[3]
        arr['manager'] = answer[4]
        cursor.close()
        return arr
api.add_resource(Random_team,'/random_team/')

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
