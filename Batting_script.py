#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2
import sys
import csv

def data(filename):
        with open(filename) as csvfile:
                reader = csv.DictReader(csvfile)
                data_list = []
                for line in reader:
                        data_list.append([line['teamID'],line['playerID'],line['yearID'],line['POS'],line['stint']])
        return data_list

con = None
eq = data('Batting.csv')

try:
        con = psycopg2.connect("dbname='mldb' user='postgres' password='hola123' host='localhost'")
        cur = con.cursor()

        for e in eq:
            if str(e[2]) == "2015":
                query = "INSERT INTO jugador_en_equipo (id_equipo,id_jugador,año,posicion,stint) VALUES ('%s','%s','%s','%s','%s')"%(str(e[0]),str(e[1]),str(e[2]),str(e[3]),str(e[4]))
                cur.execute(query)
        con.commit()
finally:
        if con:
                con.close()
